//
//  CitiesTableViewControllerTests.swift
//  RnDAssignment
//
//  Created by Arthur Alves on 03/08/2017.
//  Copyright © 2017 Arthur Alves. All rights reserved.
//

import XCTest
@testable import RnDAssignment

class CitiesTableViewControllerTests: XCTestCase {
    
    var citiesTableViewController = CitiesTableViewController()
    
    
    func testAlphatebicalSort() {
        
        let citiesByPositions = [
            0: "Andorra la Vella, AD", // Position it should appear after done sorting : City, Country
            4: "Molleres, AD",
            18: "Abu Dhabi, AE",
            209556: "Zvishavane, ZW",
            14387: "Denver, US"
        ]
        
        citiesTableViewController.cities = citiesTableViewController.loadCities()
        citiesTableViewController.sortAlphabetically()
        
        
        // ----------------
        // Test with invalid index
        //      "Denver, US" should NOT be on the position 14387 after sorting
        //
        XCTAssertNotEqual("\(citiesTableViewController.cities[14387].name), \(citiesTableViewController.cities[14387].country)", citiesByPositions[14387])
        
        // ----------------
        // Test with valid indexes
        //      "Andorra la Vella, AD" SHOULD be on the position 0 after sorting
        //      "Molleres, AD" SHOULD be on the position 4 after sorting
        //      "Abu Dhabi, AE" SHOULD be on the position 18 after sorting
        //      "Zvishavane, ZW" SHOULD be on the position 209556 after sorting
        //
        XCTAssertEqual("\(citiesTableViewController.cities[0].name), \(citiesTableViewController.cities[0].country)", citiesByPositions[0])
        XCTAssertEqual("\(citiesTableViewController.cities[4].name), \(citiesTableViewController.cities[4].country)", citiesByPositions[4])
        XCTAssertEqual("\(citiesTableViewController.cities[18].name), \(citiesTableViewController.cities[18].country)", citiesByPositions[18])
        XCTAssertEqual("\(citiesTableViewController.cities[209556].name), \(citiesTableViewController.cities[209556].country)", citiesByPositions[209556])
    }
    
    func testSearchResult() {
        
        let citiesByPositions = [
            0: "Molleres, AD",
            18: "Abu Dhabi, AE",
            209556: "Zvishavane, ZW",
            14387: "Bradvari, BG"
        ]
        
        citiesTableViewController.cities = citiesTableViewController.loadCities()
        citiesTableViewController.sortAlphabetically()
        citiesTableViewController.filteredCities = citiesTableViewController.cities
        citiesTableViewController.filterByName(searchString: "Moll", completion: {
        
            // ----------------
            // Test with invalid index
            //
            XCTAssertNotEqual("\(self.citiesTableViewController.cities[0].name), \(self.citiesTableViewController.cities[0].country)", citiesByPositions[14387])
            
            // ----------------
            // Test with valid indexes
            //
            XCTAssertEqual("\(self.citiesTableViewController.filteredCities[0].name), \(self.citiesTableViewController.filteredCities[0].country)", citiesByPositions[0])
        
        })
        
    }
    
    func testSortPerformance() {
        super.measure {
            self.citiesTableViewController.sortAlphabetically()
        }
    }
    
    func testFilterPerformance() {
        super.measure {
            // ----------------
            // First test result:
            // measured [Time, seconds] average: 0.000, relative standard deviation: 220.328%, values: [0.000185, 0.000010, 0.000005, 0.000005, 0.000020, 0.000004, 0.000004, 0.000004, 0.000004, 0.000005]
            self.citiesTableViewController.filterByName(searchString: "Moll", completion: {})
        }
    }

}
