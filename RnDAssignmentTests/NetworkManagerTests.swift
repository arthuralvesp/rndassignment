//
//  NetworkManagerTests.swift
//  RnDAssignment
//
//  Created by Arthur Alves on 03/08/2017.
//  Copyright © 2017 Arthur Alves. All rights reserved.
//

import XCTest
@testable import RnDAssignment

class NetworkManagerTests: XCTestCase {
    
    func testDictionaryToCityConversion() {
        
        // ----------------
        // City _id is supposed to be Int, therefore this must be nil
        //
        let brokenCity = ["name": "Oz", "_id": 11110.0, "country": "Magic Land",
                          "coord": ["lat": 21,"lon": 20]] as [String : Any]
        XCTAssertNil(NetworkManager.dictionaryObjToCity(object: brokenCity))
        
        
        // ----------------
        // This test is supposed to return a valid City struct
        //
        let city = ["name": "Oz", "_id": 11110, "country": "Magic Land",
                    "coord": ["lat": 21,"lon": 20]] as [String : Any]
        XCTAssertNotNil(NetworkManager.dictionaryObjToCity(object: city))
        
    }
    
    // ----------------
    // Check if all Cities in the json file are properly converted to City struct
    // Total should always be 209557
    //
    func testLoadAllCities() {
        
        XCTAssertNotEqual(NetworkManager.loadCitiesFromLocalJSON()?.count, 0)
        XCTAssertEqual(NetworkManager.loadCitiesFromLocalJSON()?.count, 209557)
    }

}
