//
//  CitiesTableViewControllerRoutes.swift
//  RnDAssignment
//
//  Created by Arthur Alves on 03/08/2017.
//  Copyright © 2017 Arthur Alves. All rights reserved.
//

import UIKit

extension CitiesTableViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mapSegue" {
            
            if let mapViewController = segue.destination as? MapViewController {
                
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    mapViewController.coords = self.filteredCities[indexPath.row].coords
                    mapViewController.title = "\(self.filteredCities[indexPath.row].name), \(self.filteredCities[indexPath.row].country)"
                    
                }
            }
        }
    }
    
}
