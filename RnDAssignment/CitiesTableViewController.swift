//
//  CitiesTableViewController.swift
//  RnDAssignment
//
//  Created by Arthur Alves on 03/08/2017.
//  Copyright © 2017 Arthur Alves. All rights reserved.
//

import UIKit

class CitiesTableViewController: UITableViewController, UISearchBarDelegate {
    
    var cities = [City]()
    var filteredCities = [City]()
    var searchBar = UISearchBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSearchBar()
        
        self.cities = loadCities()
        sortAlphabetically()
        self.filteredCities = self.cities
    }
    
    func loadCities() -> [City] {
        if let cities = NetworkManager.loadCitiesFromLocalJSON() {
            return cities
        }
        return []
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

// MARK - Sort Algorithms
extension CitiesTableViewController {
    
    func sortAlphabetically() {
        self.cities.sort {
            ($0.country, $0.name) <
                ($1.country, $1.name)
        }
    }
    
    func filterByName(searchString: String, completion: @escaping () -> ()) {
        let queue = DispatchQueue.global()
        queue.async() {
            self.filteredCities = self.cities.filter {
                $0.name.hasPrefix(searchString)
            }
            DispatchQueue.main.async(execute: completion)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.filteredCities = self.cities
            self.tableView.reloadData()
        } else {
            filterByName(searchString: searchText, completion: { self.tableView.reloadData()})
        }
    }
    
}

// MARK - Search Bar / UI Extension
extension CitiesTableViewController {
    
    func setupSearchBar() {
        self.searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40))
        self.searchBar.delegate = self
        self.searchBar.inputAccessoryView = addDoneButtonOnKeyboard()
        self.tableView.sectionHeaderHeight = CGFloat(40)
    }
    
    func addDoneButtonOnKeyboard() -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(CitiesTableViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }
    
    func doneButtonAction() {
        self.view.endEditing(true)
    }
    
}

// MARK - Table View
extension CitiesTableViewController {
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.searchBar
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "mapSegue", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredCities.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "cityCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: identifier) {
            
            if var textLabelFrame = cell.textLabel?.frame {
                textLabelFrame.origin.x = 100
                cell.textLabel?.frame = textLabelFrame;
            }
            
            cell.textLabel?.text = "\(self.filteredCities[indexPath.row].name), \(self.filteredCities[indexPath.row].country)"
            return cell
        }
        return UITableViewCell()
    }
}
