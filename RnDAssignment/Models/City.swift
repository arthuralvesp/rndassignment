//
//  City.swift
//  RnDAssignment
//
//  Created by Arthur Alves on 03/08/2017.
//  Copyright © 2017 Arthur Alves. All rights reserved.
//

import Foundation

struct Coord {
    let lat: Double
    let lon: Double
}

struct City {
    let id: Int
    let name: String
    let country: String
    let coords: Coord
}
