//
//  MapViewController.swift
//  RnDAssignment
//
//  Created by Arthur Alves on 03/08/2017.
//  Copyright © 2017 Arthur Alves. All rights reserved.
//

import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    var coords = Coord(lat: 0.0, lon: 0.0)
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMap()
    }
    
    func setupMap() {
        let center = CLLocationCoordinate2D(latitude: coords.lat, longitude: coords.lon)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        mapView.setRegion(region, animated: true)
    }
}
