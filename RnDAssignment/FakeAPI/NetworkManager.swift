//
//  NetworkManager.swift
//  RnDAssignment
//
//  Created by Arthur Alves on 03/08/2017.
//  Copyright © 2017 Arthur Alves. All rights reserved.
//

import Foundation

class NetworkManager {
    
    static func loadCitiesFromLocalJSON() -> [City]? {
        
        do {
            if let file = Bundle.main.url(forResource: "cities", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let objects = json as? [[String: Any]] {
                    
                    // JSON is a dictionary as expected
                    var cities = [City]()
                    for object in objects {
                        if let city = dictionaryObjToCity(object: object) {
                            cities.append(city)
                        }
                    }
                    return cities
                    
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("No file found")
            }
        } catch {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    static func dictionaryObjToCity(object: [String: Any]) -> City? {
        
        if let name = object["name"] as? String,
            let id = object["_id"] as? Int,
            let country = object["country"] as? String,
            let coord = object["coord"] as? [String: Any] {
            
            var lat = 0.0
            var lon = 0.0
            if coord["lat"] as? Double == nil {
                if coord["lat"] as? Int == nil {
                    return nil
                } else {
                    lat = Double((coord["lat"] as? Int)!)
                }
                
            } else {
                lat = (coord["lat"] as? Double)!
            }
            
            if coord["lon"] as? Double == nil {
                if coord["lon"] as? Int == nil {
                    return nil
                } else {
                    lon = Double((coord["lon"] as? Int)!)
                }
                
            } else {
                lon = (coord["lon"] as? Double)!
            }
            
            
            return City(id: id, name: name, country: country, coords: Coord(lat: lat, lon: lon))
            
        }
        
        return nil
    }
}
